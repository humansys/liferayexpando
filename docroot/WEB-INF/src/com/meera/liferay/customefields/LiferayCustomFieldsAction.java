package com.meera.liferay.customefields;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

/**
 * Portlet implementation class LiferayCustomFieldsAction
 */
public class LiferayCustomFieldsAction extends MVCPortlet {
	public void updateUserCustomeAttributes(ActionRequest actionRequest, ActionResponse actionResponse)throws IOException, PortletException, PortalException, SystemException {
		System.out.println("updateUserCustomeAttributes");
		ServiceContext serviceContext = ServiceContextFactory.getInstance(User.class.getName(), actionRequest);
		long currentUserId = ParamUtil.getLong(actionRequest,"currentUserId");
		User user=UserLocalServiceUtil.getUser(currentUserId);
		System.out.println("updateUserCustomeAttributes111"+user);
		user.setExpandoBridgeAttributes(serviceContext);
		UserLocalServiceUtil.updateUser(user);
	}

}
