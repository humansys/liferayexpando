<%@page import="com.liferay.portal.kernel.workflow.WorkflowConstants"%>
<%@page import="com.liferay.portal.kernel.search.Hits"%>
<%@page import="com.liferay.portal.kernel.search.Sort"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portlet.expando.service.ExpandoValueLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@ include file="init.jsp" %>
<portlet:renderURL  var="updatePage">
<portlet:param name="mvcPath" value="/html/liferaycustomfieldsaction/update_user_custom_fields.jsp"/>
</portlet:renderURL>
<portlet:renderURL  var="viewUserCustomeFields">
<portlet:param name="mvcPath" value="/html/liferaycustomfieldsaction/view_user_custom_fields.jsp"/>
</portlet:renderURL>
<h2>Liferay Custom Fields Portlet</h2>
<%if(themeDisplay.isSignedIn()){ %>
<aui:a href="<%=updatePage%>" label="Update User Custom Fields"></aui:a><br/>
<aui:a href="<%=viewUserCustomeFields%>" label="View User Custom Fieds"></aui:a><br/>
		
<%}else{%>
<h2>Please Sign In and Use this portlet</h2>
<%}%>