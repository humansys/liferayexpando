<%@page import="com.liferay.portal.model.User"%>
<%@ include file="init.jsp" %>
<%
User selUser=themeDisplay.getUser();

%>
<portlet:actionURL  var="updateUserCustomeAttributeURL" name="updateUserCustomeAttributes">
</portlet:actionURL>
<aui:form action="<%= updateUserCustomeAttributeURL %>" method="post" name="fm">
<aui:input name="currentUserId" value="<%=user.getUserId()%>" type="hidden"/>
			<liferay-ui:custom-attribute-list
			className="<%= User.class.getName() %>"
			classPK="<%= selUser != null ? selUser.getUserId() : 0 %>"
			editable="<%= true %>" label="true"/>
<aui:button type="submit" value="update"/>
</aui:form>	
		
	